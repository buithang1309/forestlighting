﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;

public class BloomSlider : UdonSharpBehaviour
{
    public Slider slider;
    public PostProcessVolume bloomVolume;
    void Start()
    {
        slider.SetValueWithoutNotify(bloomVolume.weight);
    }

    public void _SetBloom()
    {
        bloomVolume.weight = slider.value;
    }
}
