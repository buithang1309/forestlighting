﻿
using UdonSharp;
using UnityEngine;
using VRC.SDKBase;
using VRC.Udon;

public class MirrorController : UdonSharpBehaviour
{
    public bool isLow;
    public bool isHigh;
    public bool isToggleEffect;
    public GameObject mirrorLow;
    public GameObject mirrorHigh;
    public GameObject effectObject;
    public void Interact()
    {
        if (isLow)
        {
            mirrorLow.SetActive(!mirrorLow.activeInHierarchy);
            mirrorHigh.SetActive(false);
        }else if (isHigh)
        {
            mirrorHigh.SetActive(!mirrorHigh.activeInHierarchy);
            mirrorLow.SetActive(false);
        }else if (isToggleEffect)
        {
            effectObject.SetActive(!effectObject.activeInHierarchy);
        }
    }
}
